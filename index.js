// Unicode code points: https://w3c.github.io/smufl/latest/tables/individual-notes.html
const symbols = [ "&#xE1DF;", "&#xE1DD;", "&#xE1DB;", "&#xE1D9;", "&#xE1D7;", "&#xE1D5;", "&#xE1D3;", "&#xE1D2;", "&#xE1D0;" ];

function get_symbol(duration, index) {
	let res_duration = 0;
	let res_symbols = [];
	let res_durations = [];
	// Invariant: sum(res_durations) = res_duration

	while (res_duration < duration) {
		for(let i = symbols.length - 1; i >= index; i--) {
			let d = (1 << (i - index));
			if (d > duration - res_duration) continue;
			if(res_durations.length > 0 && d === res_durations[res_durations.length - 1] / 2) {
				res_symbols[res_symbols.length - 1] += "&#xE1E7;";
			} else {
				res_symbols.push(symbols[i]);
			}
			res_durations.push(d);
			res_duration += d;
			break;
		}
	}
	return res_symbols.join("&#x203F;");
}

var beats = [];

function render() {
	// Compute the durations from the input beats
	let durations = [];
	for (let i = 0; i < beats.length - 1; i++)
		durations.push(beats[i + 1] - beats[i]);

	// Map the durations to integer multiples of the shortest one
	const min = Math.min(...durations);
	durations = durations.map(duration => Math.round(duration / min));
	document.getElementById("durations").innerHTML = durations.join(" ");
	console.log(durations);

	// Map the durations to the correct Unicode characters
	notes = durations.map(duration => get_symbol(
		duration,
		parseInt(document.getElementById("unit").value),
	));
	document.getElementById("notes").innerHTML = notes.join("&emsp;");
}

document.addEventListener("keyup", function(event) {
	if (event.code == "Space") {
		beats.push(Date.now());
		render();
	} else if (event.code == "Enter") {
		beats = [];
		render();
	}
});

document.addEventListener("touchend", function(event) {
	beats.push(Date.now());
	render();
});

